#ifndef WINDOW_H
#define WINDOW_H

#include <SFML\Graphics.hpp>
#include <iostream>
#include "World.h"

class Window {

public:

	Window(unsigned int width, unsigned int height,std::string name);
	~Window();

	int Setup();
	void Start();

private:

	unsigned int windowWidth, windowHeight;
	std::string windowName;
	sf::RenderWindow window;

	int k = 1;

	// sky
	sf::Texture skybox;
	sf::Sprite sky;

	// tiles
	sf::Texture tileTexture, wallTexture,grassTexture;
	sf::Sprite tiles , walls, grass;

	// timer
	sf::Clock clock;
	sf::Time elapsed;
	
	World world;
	array2D map;
};


#endif // !WINDOW_H
