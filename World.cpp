#include "World.h"

World::World() {

}

int World::initializeMap() {


	for (int x = 0; x < MAX_TILES_W; x++) {
		for (int y = 0; y < MAX_TILES_H; y++) {
			if (d(e) < CHANCE_ALIVE) {
				cellmap[x][y] = 1;
			}
			else {
				cellmap[x][y] = 0;
			}
		}
	}

	

	return 0;
}

int World::testMap() {

	std::cout << "********************************" << std::endl;
	std::cout << "World::testMap function output:" << std::endl;
	std::cout << "********************************" << std::endl;

	for (int x = 0; x < MAX_TILES_W; x++) {
		for (int y = 0; y < MAX_TILES_H; y++) {
			if (cellmap[x][y] == false) {
				std::cout << "0";
			}
			else if (cellmap[x][y] == true) {
				std::cout << "1";
			}
			else {
				
			}
			
		}
		std::cout << "\n";
	}
	return 0;
}


array2D World::getWorld(array2D &array) {
	
	std::vector<int> temp;

	for (int i = 0; i < MAX_TILES_W; i++) {
		for (int j = 0; j < MAX_TILES_H; j++) {
			temp.push_back(cellmap[i][j]);
		}
		array.push_back(temp);
		temp.clear();
	}

	std::cout << "********************************" << std::endl;
	std::cout << "World::getWorld function output:" << std::endl;
	std::cout << "********************************" << std::endl;
	for (int i = 0; i < MAX_TILES_W; i++) {
		for (int j = 0; j < MAX_TILES_H; j++) {
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}

	return array;
}

int World::countAliveNeighbours(array2D &array, int x, int y) {

	int count = 0;

	for (int i = -1; i < 2; i++) {
		for (int j = -1; j < 2; j++) {
			int neighbour_x = x + i;
			int neighbour_y = y + j;
			
			if (i == 0 && j == 0) {
				// 123
			}
			// edge
			else if (neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= MAX_TILES_W || neighbour_y >= MAX_TILES_H) {
				count = count + 1;
			}
			
			else if (array[neighbour_x][neighbour_y]) {
				count = count + 1;
			}

		}
	}
	return count;
}

array2D World::evolutionStep(array2D &oldWorld, int deathLimit, int birthLimit) {

		array2D newWorld = oldWorld;
		
		for (int x = 0; x < MAX_TILES_W; x++) {
			for (int y = 0; y < MAX_TILES_H; y++) {
				int aliveNeighbours = countAliveNeighbours(oldWorld, x, y);

				//First, if a cell is alive but has too few neighbours, kill it.
				if (oldWorld[x][y]) {
					if (aliveNeighbours < deathLimit) {
						newWorld[x][y] = 0;
					}
					else {
						newWorld[x][y] = 1;
					}
				} //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
				else {
					if (aliveNeighbours > birthLimit) {
						newWorld[x][y] = 1;
					}
					else {
						newWorld[x][y] = 0;
					}
				}
			}
		}

		return newWorld;
	
}


World::~World() {

}